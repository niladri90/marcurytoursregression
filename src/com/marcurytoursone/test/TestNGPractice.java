package com.marcurytoursone.test;

import java.awt.AWTException;
import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.sikuli.script.FindFailed;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class TestNGPractice {
	
	@BeforeMethod

	public void browserappaunch() {

		MethodRepository.appLaunch();

	}
	
	@Test(priority=1,enabled=true)
	
	public void verifiedvalidLogin() throws FindFailed, AWTException, InterruptedException, IOException {
		
		
		MethodRepository.verifyLogin();
		
	}
		
	
	
	
	@Test(priority=2,enabled=false)
	
	public void verifiedinvalidLogin() {
		
		
		MethodRepository.verifyinvalidLogin();
	}
	
	@AfterMethod
	
	public void cleanUp() {
		
		MethodRepository.closeBrowser();
		
	}
}
