package com.marcurytoursone.test;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;

public class MethodRepository {

	static WebDriver driver;

	public static void appLaunch() {

		// Driver initialization

		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		driver = new ChromeDriver();

		driver.manage().window().maximize();

		driver.get("http://newtours.demoaut.com/");

		// driver.close();
	}

	public static void verifyLogin() throws AWTException, FindFailed, IOException {

		WebElement uname = driver.findElement(By.name("userName"));
		WebElement password = driver.findElement(By.name("password"));

		// WebElement uname = driver.findElement(By.xpath("//input[@name='userName']"));

		// uname.sendKeys("dasd");

		/*
		 * Screen screen = new Screen(); Pattern username = new
		 * Pattern("./images/username2.png"); screen.wait(username,10);
		 * screen.type(username,"dasd");
		 * 
		 * WebElement password = driver.findElement(By.name("password"));
		 * //password.sendKeys("dasd"); Pattern password = new
		 * Pattern("./images/password.png"); //screen.wait(password,10);
		 * screen.type(password,"dasd");
		 */

		Properties obj = new Properties();
		FileInputStream objfile = new FileInputStream("./Testdata/Testdata.properties");
		obj.load(objfile);
		String Username = obj.getProperty("Username");
		uname.sendKeys(Username);
		String Password = obj.getProperty("Password");
		password.sendKeys(Password);

		Actions builder = new Actions(driver);

		WebElement btnsignin = driver.findElement(By.name("login"));

		builder.moveToElement(btnsignin).click().perform();
		// btnsignin.click();

		/*
		 * Robot btnenter = new Robot(); btnenter.keyPress(KeyEvent.VK_ENTER);
		 * btnenter.keyRelease(KeyEvent.VK_ENTER);
		 */

		String exptitle = "Find a Flight: Mercury Tours:";

		String actualtitle = driver.getTitle();

		if (exptitle.equals(actualtitle)) {

			System.out.println("Login Successfully");
		} else {

			System.out.println("Login Error");
		}

	}

	public static void verifyinvalidLogin() {

		WebElement uname = driver.findElement(By.xpath("//input[@name='userName']"));

		uname.sendKeys("das");

		WebElement password = driver.findElement(By.name("password"));
		password.sendKeys("das");

		Actions builder = new Actions(driver);

		WebElement btnsignin = driver.findElement(By.name("login"));

		builder.moveToElement(btnsignin).click().perform();

		// Sign-on: Mercury Tours

		String exptitle = "Sign-on: Mercury Tours";

		String actualtitle = driver.getTitle();

		if (exptitle.equals(actualtitle)) {

			System.out.println("Not Logged in Successfully");
		} else {

			System.out.println("Login Error");
		}

	}

	public static void verifyPassengers() {
		
		String expecteddepartingfrom ="London";

		WebElement dropdown = driver.findElement(By.name("fromPort"));

		Select departingfrom = new Select(dropdown);

		departingfrom.selectByVisibleText("London");
		
		String actualdepartingFrom = dropdown.getAttribute("value");
		
		System.out.println(actualdepartingFrom);
		
		if(expecteddepartingfrom.equals(actualdepartingFrom)){
			
			System.out.println("Test Case is passed");
			
		}
		else {
			
			System.out.println("Test Case is failed");
		}
		
		

		// departingfrom.selectByIndex(2);

		// departingfrom.selectByValue("Paris");

	}

	public static void selectedType() {

		Boolean type = driver.findElement(By.xpath("//input[@value='roundtrip']")).isSelected();

		if (type == true) {

			System.out.println("Round Trip is showing as selected");

		}

		else {

			System.out.println("Round Trip is not showing as selected");

		}

	}

	public static void fileupload() throws InterruptedException, IOException {

		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		driver = new ChromeDriver();

		driver.manage().window().maximize();

		driver.get("https://files.fm/");
		WebElement uploadfilebutton = driver.findElement(By.id("uploadifive-file_upload"));

		uploadfilebutton.click();

		Thread.sleep(3000);
		Runtime.getRuntime().exec("./Files/Fileupload.exe");

		// WebElement startfileupload = driver.findElement(By.id("savefiles"));

		// startfileupload.click();

	}

	public static void webtablehandling() {

		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		driver = new ChromeDriver();

		driver.manage().window().maximize();

		driver.get("https://money.rediff.com/gainers/bsc/daily/groupa");

		List col = driver.findElements(By.xpath(".//*[@id=\"leftcontainer\"]/table/thead/tr/th"));
		System.out.println("No of cols are : " + col.size());

		List rows = driver.findElements(By.xpath(".//*[@id='leftcontainer']/table/tbody/tr/td[1]"));
		System.out.println("No of rows are : " + rows.size());

	}

	public static void closeBrowser() {

		System.out.println("\n Closing the Browser.");
		driver.quit();
	}
}
