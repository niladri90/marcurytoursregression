package com.marcurytoursone.test;

import java.awt.AWTException;
import java.io.IOException;

import org.sikuli.script.FindFailed;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Dashboard {
	
	@BeforeClass
	
	public void driverlaunch() throws FindFailed, AWTException, IOException {
		
		MethodRepository.appLaunch();
		MethodRepository.verifyLogin();
		
		
	}
	
	@Test(priority=2)
	
	public void selectDropdown() {
		
		MethodRepository.verifyPassengers();
	}
	
	@Test(priority=1)
	
	public void selectedType() {
		
		
		MethodRepository.selectedType();
	}
	 
	
	@AfterClass
	
	public void driverquit() {
		
		MethodRepository.closeBrowser();
	}
	
}
	

